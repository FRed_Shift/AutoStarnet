import subprocess
import sys
import os
from PIL import Image
from pysiril.siril import Siril
from pysiril.addons import Addons
from pysiril.wrapper import Wrapper
import pathlib
from astropy.io import fits


def Run(*args, trailer= '_s', stride= '256', keep_open=True ):   
  
    #preparing pysiril
    workdir = os.getcwd()
    print('Starting PySiril')
    app = Siril()
    AO = Addons(app)
    cmd = Wrapper(app)
    app.tr.Configure(False,False,True,True)
    app.MuteSiril(True)
    app.Open()
    print('Starting Siril')
        
    for file_in in args:
        print('\nFile to process :', str(file_in))
        #Get attributes (name and extension) of the input file
        filename, inputfile_extension=os.path.splitext(file_in)
        
        if inputfile_extension!='.fits' and inputfile_extension!='.fit': 
            print ('ABORTED: Not a FIT/FITS file')
            return
    
        #Get the FILTER value of the FITS header
        hdul = fits.open(file_in, mode='update')
        try:
            fltr0=hdul[0].header['FILTER']        
        except Exception as e :
            print("\n******* " +  str(e) + "in "+ str(file_in))
            print("*******  Creating the missing field... " + "\n" )
            hdr= hdul[0].header
            hdr.insert('DATE', ('FILTER', '   '))
            hdul.flush()
            fltr0=hdul[0].header['FILTER']
        print("FILTER = " +  str(fltr0) + "\n" )
        hdul.close(output_verify='fix')
        
        #Set pysiril
        app.Execute('setext {:s}'.format(inputfile_extension))
        app.Execute('cd {:s}'.format(workdir))

        # Open the original input 32b FITS file
        # convert it into 16b and save as TIF file with the same name
        cmd.load(filename)          
        cmd.set16bits
        cmd.savetif(filename)

        #Define the 16b version of the initial file, used as input for Starnet++
        filename_tif = filename + '.tif'   
    
        #Get attributes (name and extension) of the last file
        fileroot, file_extension=os.path.splitext(filename_tif)
    
        #Define the output file of Starnet++
        outputfile_extension=".tif"
        outputfilename=filename+trailer+outputfile_extension
        
        
        #String Compatibility test
        if ' ' in filename_tif:
            print ('FAILURE: No space allowed in file name or file path')
            return

            
        #Define image mode
        im = Image.open(filename_tif)
        imode=im.mode
        im.close()
        need_stars = True
        if imode=='I;16': 
            need_stars = False
        
##Few results of "mode" method according to file format input
##          color   Mono
##fits 32     F       F
##fits 16     F       F
##fits 16u    F       F
##fits 8      F       F
##Tif 32      /       F
##Tif 16      RGB     I;16  (the only file format unsable by Starnet)
##Tif 8       RGB     L


        #Compatibility test
        if ((imode=='I;16' or imode=='RGB') and file_extension=='.tif') :
            #set argument for Starnet++
            args= "starnet++.exe " + filename_tif +" "+ outputfilename + " " + stride
            print ('Starnet++ is running... ')
            subprocess.call(args, shell=True)
        else: 
            print ('Not a TIF/16bit file')
            return
    
        #Convert the result starless file into a 32b version 
        starless_filename = fileroot + '_s'
        cmd.load(starless_filename)             #Load bla_s.tif
        cmd.set32bits
        cmd.fmul('1.0')
        cmd.save(starless_filename)             #Save bla_s.fits
    
        #Creation of the Stars.fits
        if need_stars == True:
            folder,_=os.path.split(starless_filename)
            #stars=folder+"\\" + fileroot + "_stars.fits"
            stars=filename+"_stars"
            cmd.load(file_in)                 #Load bla.FITS, stars+neb
            cmd.isub(starless_filename)             #substract starless version
            cmd.save(stars)                         #save stars.FITS

        #Deletion of TIF temp files  
        tokill=starless_filename + '.tif'         
        os.remove(tokill)                       #Remove bla_s.tif  
        os.remove(filename_tif)                 #Remove bla.tif 
    
        ########################################
        ########################################
        #Set the FILTER value in the FITS header
        # At first, for the Starless file
        sl_name=starless_filename+'.fits'
        #print ('starless file2: ', sl_name)
        hdul = fits.open(sl_name, mode='update')
    
        try:
            fltr=hdul[0].header['FILTER']        
        except Exception as e :
            print("******* " +  str(e) + "in "+ str(sl_name))
            print("******* " +  " Creating the missing field... ")
            hdr= hdul[0].header
            hdr.insert('DATE', ('FILTER', ' '))
            hdul.flush()
            fltr=fltr0
    
        hdr = hdul[0].header
        newfltr= str(fltr)+'_sls' 
        hdr['FILTER'] = newfltr
        hdul.close(output_verify='fix')
    
        ########################################    
        # ...and then for the Stars file    
        if need_stars == True:
            stars=stars+'.fits'
            #print ('stars file2: ', stars)
            hdul = fits.open(stars, mode='update')
    
            try:
                fltr=hdul[0].header['FILTER']        
            except Exception as e :
                print("******* " +  str(e) + "in "+ str(stars))
                print("******* " +  " Creating the missing field... ")
                hdr= hdul[0].header
                hdr.insert('DATE', ('FILTER', ' '))
                hdul.flush()
                fltr=fltr0
      
            hdr = hdul[0].header
            newfltr= str(fltr)+'_str' 
            hdr['FILTER'] = newfltr
            hdul.close(output_verify='fix')
        ########################################  
   
    app.Close()
    del app
    print ('Stop')

if __name__ == "__main__":
    Run(*sys.argv[1:])
    