---
title: Starnett++ and Siril
author: Fred Denjean
show_author: true
featured_image: PMwindow.png
rate: "2/5"
type: page
---

This tutorial aims to present a python script allowing to simply interface Starnet++ and Siril.
This tool meets the needs of users to be able to manipulate images from Starnet++ with Siril and especially Pixel Math.

{{< table_of_contents >}}

## Why this script?
Images from Siril pre-processing are in standard FITS/32bit format.
However, the images usable by Starnet++ must be in TIFF/16bit format. They must therefore be converted.
Similarly, Starnet++ output images are in TIFF/16bit. If you want to benefit from Siril's calculation precision, you have to convert them to FITS/32bit.
This format is also the only one supported by Siril's PixelMath tool.

## Prerequisites
The goal is to use here a script written in python (that everyone can adapt), and based on the Siril ecosystem.
For this, you will first need:
- install astropy, see https://www.astropy.org/
- install pysiril, see https://siril.org/tutorials/pysiril/
- Install PIL
- Unzip the Command Line Tool version of Starnet++ (https://www.starnetastro.com/download/) in a known directory.

The python script must then be placed in this directory, just next to the executable `starnet++.exe`.

{{<figure src="Autostarnet-Folder.png" link="Autostarnet-Folder.png" caption="The script place">}}

## What does this script?
- It transforms the input files in FITS/32 into TIFF/16bit file(s)
- It launches Starnet++
- It re-transforms the result file into FITS/32
- It updates the `FILTER` field of the generated files to make them compatible with PixelMath

The file(s) only need to be "dragged" onto the Python script as shown in the video.

## The input files
- They must be of the FIT/FITS type.
- The best is to use files whose histogram has already been stretched.
- It can be a mono file (R, G or B) from a `split` command or Ha and/or OIII files from an `extract` command.
- It can be an RGB file (3 layers therefore).

{{<figure src="Input.png" link="Input.png" caption="Example of input files set">}}

## The output files 
At the output of the script, 2 possibilities:
- If the input file was mono, only the `myfile_s.fits` file is generated. `_s` for "starless" ...
- If the input file was in RGB, in addition to the `myfile_s.fits` file, the `myfile_stars.fits` file (the star mask) is also be generated.

{{<figure src="Output.png" link="Output.png" caption="Example of the corresponding output files">}}

## What about PixelMath?
The PixelMath tool recently introduced in Siril makes it possible to manage the images to be handled thanks to a sorting facilitated by the `FILTER` field of the FITS header.

This python script remains in this perspective and updates this `FILTER` field.
Thereby:
- the starless file, whose name is of the type `myfile_s.fits`, also have a field of the type `FILTER = CLS_sls` (if CLS was the original value...).
- the star mask, whose name is of type `myfile_stars.fits`, also have a field of type `FILTER = CLS_str` (if CLS was the original value...).

## Let's see an example
Here it is a mix of mono images (`R.fits`, `G.fits`, `B.fits`) and an RGB image (`result2-CLS.fits`) made with a CLS filter (and therefore `FILTER=CLS`).

These 4 images are dragged simultaneously onto the script, and after several minutes of calculation, we see the resulting files.

<video controls width=100%>
  <source src="Process.webm" type="video/webm">
Your browser does not support the video tag.
</video> 

Then, by opening Siril and PixelMath, one can realize the efficiency of sorting images according to the updated `FILTER` values.
This effect is seen on mono images, then on RGB images.

<video controls width=100%>
  <source src="PMStarnet.webm" type="video/webm">
Your browser does not support the video tag.
</video> 


It's up to you now!!!
